class AddFileToUploadedFiles < ActiveRecord::Migration
  def change
    add_column :penn_station_uploaded_files, :file, :string
    add_column :penn_station_uploaded_files, :content_type, :string
    add_column :penn_station_uploaded_files, :size, :integer
  end
end
