class CreatePennStationFolders < ActiveRecord::Migration
  def change
    create_table :penn_station_folders do |t|
      t.string :name

      t.timestamps
    end
  end
end
