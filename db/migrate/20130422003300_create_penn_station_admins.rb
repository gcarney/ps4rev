class CreatePennStationAdmins < ActiveRecord::Migration
  def change
    create_table :penn_station_admins do |t|
      t.string :email
      t.string :name
      t.string :password_digest

      t.timestamps
    end
  end
end
