# Reverse Engineered Branch of PennStation 4

This is based on the portion of a version called 4.0.0.rc7ga that got packaged
up as a gem and included in the code for the Knott Fdn. web site. The "ga"
probably means Google Analytics. Jack did not find the version in the PS4 repo,
so here he extracts it from the .gem.

Whereas the usual method for packaging up PS4 as a gem would be to say
"bundle exec rake build", none of that is supported here, so the method here is:

  gem build penn_station.gemspec