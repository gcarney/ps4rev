
# see this page for engines with rspec, factory_girl and capybara
# http://viget.com/extend/rails-engine-testing-with-rspec-capybara-and-factorygirl

$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "penn_station/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "penn_station"
  s.version     = PennStation::VERSION
  s.authors     = ["Warren Vosper", "Charles Melhorn", "Jack Waugh"]
    # Listing Jack for blame, not credit.
  s.email       = ["jackw@cctsbaltimore.org"]
  s.homepage    = "http:://cctsbaltimore.org"
  s.summary     = "PennStation Content Management System (CMS)."
  s.description = "PennStation as a rails engine."

  s.files = Dir[
    "{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc",
    "penn_station.gemspec"
  ]
  s.test_files = Dir["spec/**/*"]

  s.add_runtime_dependency(%q<rails>, ["~> 4.0.0"])
  s.add_runtime_dependency(%q<jquery-rails>, [">= 0"])
  s.add_runtime_dependency(%q<bcrypt-ruby>, ["~> 3.1.2"])
  s.add_runtime_dependency(%q<dynamic_form>, [">= 0"])
  s.add_runtime_dependency(%q<rubytree>, [">= 0"])
  s.add_runtime_dependency(%q<tinymce-rails>, ["= 3.5.8.2"])
  s.add_runtime_dependency(%q<sass-rails>, ["~> 4.0.0"])
  s.add_runtime_dependency(%q<dropzonejs-rails>, [">= 0"])
  s.add_runtime_dependency(%q<rmagick>, [">= 0"])
  s.add_runtime_dependency(%q<carrierwave>, ["= 0.8.0"])
  s.add_runtime_dependency(%q<legato>, ["= 0.3.0"])
  s.add_runtime_dependency(%q<oauth2>, ["= 0.9.3"])
  s.add_development_dependency(%q<rspec-rails>, [">= 0"])
  s.add_development_dependency(%q<capybara>, [">= 0"])
  s.add_development_dependency(%q<factory_girl_rails>, [">= 0"])
  s.add_development_dependency(%q<pry>, [">= 0"])
  s.add_development_dependency(%q<pry-plus>, [">= 0"])
end
