jQuery(function() {

  var analytics_setting_id = jQuery('#analytics_id').attr('data-id');

  if ( !(analytics_setting_id === undefined) )
   { 
     jQuery.ajax({type: "GET",
                  url: "/admin/analytics/"+ analytics_setting_id,
                  dataType: 'html',
                  success: function(data, status, xhr) {$('#analytics_data').empty().append(data)}
     })
   }

});
