
jQuery(function() {
  jQuery('.sortable').nestedSortable({
                        handle: 'div',
                        isTree: true,
                        items: 'li',
                        toleranceElement: '> div',
                        update: function(event,ui) {
                                   jQuery.ajax({
                                     evalScripts: true,
                                     url: '/admin/pages/reorder',
                                     type: "GET",
                                     data: jQuery('.sortable').nestedSortable('serialize'),
                                     dataType: 'script',

//                                     beforeSend: function(ev,xhr) {
//                                                   alert("das update" + jQuery('.sortable').nestedSortable('serialize'));
//                                                 },

                                     complete:   function() {
                                                 },

//                                     error:      function(jqXHR, textStatus, errorThrown) {
//                                       alert(textStatus);
//                                                   alert("There was a problem reordering the list.  Please refresh the page and try again.");
//                                                 }
                                   });
                                 }
                         });
});
