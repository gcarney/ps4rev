jQuery(function() {

  jQuery('#pennstation .panels .current').first().show();

  jQuery('#pennstation #panel_tabs li').click(function() {
    var panel = jQuery(this).attr('data-panel-id');

    jQuery('#pennstation #panel_tabs li.current').toggleClass('current');
    jQuery(this).toggleClass('current');

    jQuery('#pennstation .panels .current').first().toggleClass('current').hide();
    jQuery('#pennstation .panels #' + panel).toggleClass('current').show();
  });

  jQuery('#pennstation #panel_ops li#save').click(function() {
    var form = jQuery('#pennstation .panels .current form').first();
    var input = $("<input>").attr("type", "hidden")
                            .attr("name", "save_op")
                            .val("save");

    form.append($(input));
    form.submit();
    event.stopPropagation();
  });

  jQuery('#pennstation #panel_ops li#save_and_preview').click(function() {
    var form = jQuery('#pennstation .panels .current form').first();
    var input = $("<input>").attr("type", "hidden")
                            .attr("name", "save_op")
                            .val("preview");

    form.append($(input));
    form.submit();
    event.stopPropagation();
  });

});
