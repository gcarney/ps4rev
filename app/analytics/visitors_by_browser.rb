class VisitorsByBrowser
  extend Legato::Model

  metrics :visitors
  dimensions :browser, :browserVersion
end


