module PennStation
  class Section < ActiveRecord::Base
    validates :content, presence: true

    validates :name, presence: true

    before_validation :set_defaults, :on => :create

    belongs_to :page

    def activity_info
      "page #{page.title}, section #{name}"
    end

    private

    def set_defaults
      self.content = "<h1>#{name}</h1>"
      true
    end
  end
end
