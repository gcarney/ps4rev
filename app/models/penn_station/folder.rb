module PennStation
  class Folder < ActiveRecord::Base
    validates :name, presence: true,
                     uniqueness: true

    has_many  :uploaded_files

    def activity_info
      "folder #{name}"
    end
  end
end
