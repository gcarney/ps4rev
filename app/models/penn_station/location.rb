
module PennStation
  class Location < ActiveRecord::Base
    belongs_to :event, class_name: "::PennStation::Event"
    belongs_to :address, class_name: "::PennStation::Address"

    accepts_nested_attributes_for :address
  end
end
