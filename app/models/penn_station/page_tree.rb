require 'rubytree'

module PennStation

  class PageTree
    attr_accessor :root

    def initialize
      self.root = Tree::TreeNode.new("/", Page.lookup('/'))
      build_tree
      self
    end

    def build_tree
      Page.all.sort{|a,b| a.url <=> b.url}.each do |page|
        current_node = root
        page.url.split('/').each do |url_component|
          next if url_component.blank?
          current_node = add_child(current_node,url_component,page)
        end
      end
    end

    def add_child(root,name,content)
      begin root << Tree::TreeNode.new(name,content) rescue nil end
      root[name]
    end

    def find(page, start=root)
      return start if start.content.id == page.id

      found = nil
      unless start.children.empty?
        start.children.each do |child|
          found = find(page,child)
          return found if found
        end
      end
      found
    end

    def ordered_find(search=nil)
      tree = self
      target = search.nil? ? tree.root.content : search
      tree.find(target).children.select{|p| p.content.published and p.content.show_in_nav }.sort{|a,b| a.content.position <=> b.content.position}
    end

    def self.ordered_find(search=nil)
      # Purpose not documented by original author.
      self.new.ordered_find(search)
    end
  end
end
