module PennStation
  class Activity < ActiveRecord::Base
    validates :administrator, presence: true
    validates :info, presence: true
    validates :operation, presence: true

    def message
      "#{administrator} #{operation} #{info}"
    end
  end
end
