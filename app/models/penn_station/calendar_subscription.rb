
module PennStation
  class CalendarSubscription < ActiveRecord::Base
    belongs_to :calendar, class_name: "::PennStation::Calendar"
    belongs_to :page, class_name: "::PennStation::Page"
  end
end
