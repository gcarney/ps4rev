module PennStation
  class Page < ActiveRecord::Base
    attr_accessor :parent_url

    validates :position, presence: true,
                         numericality: { :greater_than_or_equal_to => 0 }

    validates :ptype, format: { :with => /\A[a-zA-Z\d]+\z/ }

    validates :published, inclusion: { :in => [true, false] }

    validates :show_in_nav, inclusion: { :in => [true, false] }

    validates :title, presence: true,
                      uniqueness: true

    validates :url, presence: true,
                    uniqueness: true

    before_validation :urlize

    before_validation :set_defaults, :on => :create

    after_find do |page|
      if ptype == 'HomePage'
        self.parent_url = ''
      else
        parent = url.split('/')[0..-2].join('/')
        parent = '/' if parent.empty?
        self.parent_url = parent
      end
    end

    has_many :sections, :dependent => :destroy

    has_many :calendar_subscriptions, class_name: "::PennStation::CalendarSubscription"
    has_many :calendars, :through => :calendar_subscriptions

    def self.lookup(url)
      self.where(url: (url == '/') ? '/' : "/#{url}").first
    end

    def self.top_level(page)
      return page if page.parent_url == '/' or page.url == '/'
      self.where(url: '/' + page.url.split('/')[1]).first
    end

    def parent
      Page.where(url: parent_url).first
    end

    def home_page?
      url == '/'
    end

    def activity_info
      "page #{title}"
    end

    private

    def urlize
      if (ptype == 'HomePage') || (title == 'HomePage')
        self.url = '/'
      else
        self.url = (parent_url || '/')
        self.url += '/' unless url[-1] == '/'
        self.url += (title || '').gsub(/ /, "_").gsub(/[^\d\w\-_]/, "").downcase
      end
    end

    def set_defaults
      self.published = home_page?
      self.show_in_nav = true
      self.sections << Section.new(name: 'main', content: '<h1>main</h1>')
      self.position = 0 if position.nil?

      default_page_type
      true
    end

    def default_page_type
      if home_page?
        self.ptype = 'HomePage'
      else
        self.ptype ||= 'Page'
      end
    end
  end
end
