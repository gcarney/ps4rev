
# http://asciicasts.com/episodes/270-authentication-in-rails-3-1

module PennStation
  class Admin < ActiveRecord::Base
    has_secure_password

    validates :email, presence: true,
                      uniqueness: true

    validates :name, :presence => true,
                     :uniqueness => true

    validates :password, :length => { :minimum => 8, :if => :validate_password? },
                         :confirmation => { :if => :validate_password? }

    validates :password_confirmation, :length => { :minimum => 8, :if => :validate_password? },
                         :confirmation => { :if => :validate_password? }

    has_many :roles, autosave: true

    accepts_nested_attributes_for :roles, allow_destroy: true

    def activity_info
      "administrator #{name}"
    end

    def has_role?(*check_against)
      not (roles.map(&:name) & check_against).empty?
    end

    private

    def validate_password?
      password.present? || password_confirmation.present?
    end
  end
end
