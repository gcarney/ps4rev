require_dependency "penn_station/application_controller"

module PennStation
  class AdminsController < ApplicationController
    before_action :set_admin, only: [:show, :edit, :update, :destroy]

    track_admin_activity

    def index
      @admins = Admin.order(:name)
    end

    def show
    end

    def new
      @admin = Admin.new
      generate_roles_list
    end

    def edit
      generate_roles_list
    end

    def create
      @admin = Admin.new(admin_params)

      if @admin.save
        redirect_to @admin, notice: "Administrator #{@admin.name} was successfully created."
      else
        generate_roles_list
        render action: 'new'
      end
    end

    def update
      if @admin.update(admin_params)
        redirect_to edit_admin_path(@admin), notice: "Successfully updated Administrator #{@admin.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @admin.destroy
      redirect_to admins_url, notice: "Administrator #{@admin.name} was successfully deleted."
    end

    private

    def set_admin
      @admin = Admin.find(params[:id])
    end

    def admin_params
      plist = params.require(:admin).permit(:name, :email, :password, :password_confirmation, roles_attributes: [:id,:name])

        # mark any associated roles that were unchecked for destruction.

      if plist.has_key?("roles_attributes")
        plist["roles_attributes"].each do |k,v|
         v["_destroy"] = true unless v.has_key?("name")
        end
      end

      plist
    end

    def generate_roles_list
      (PennStation::Role.available - @admin.roles.map(&:name)).each do |available_role|
        next unless current_admin.has_role?(available_role)
        @admin.roles.build(name: available_role)
      end
    end
  end
end
