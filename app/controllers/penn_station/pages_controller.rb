require_dependency "penn_station/application_controller"

module PennStation
  class PagesController < ApplicationController
    before_action :set_page, only: [:show, :edit, :update, :destroy]

    track_admin_activity

    def index
      @workspace = "pages"
      @pages = ::PennStation::PageTree.new
    end

    def show
    end

    def new
      @page = Page.new(parent_url: page_params['parent_url'], ptype: 'Page')
    end

    def edit
    end

    def create
      @page = Page.new(page_params)

      if @page.save
        if params[:save_op] == 'save'
          redirect_to edit_page_path(@page), notice: "Successfully created #{@page.title}."
        else
          redirect_to "#{@page.url}"
        end
      else
        render action: 'new'
      end
    end

    def update
      if @page.update(page_params)

          # save or save and preview.

        if params[:save_op] == 'save'
          redirect_to edit_page_path(@page), notice: "Successfully updated #{@page.title}."
        else
          redirect_to "#{@page.url}"
        end
      else
        render action: 'edit'
      end
    end

    def destroy
      @page.destroy
      redirect_to pages_url, notice: "Page #{@page.title} was successfully deleted."
    end

    def reorder

      # {"page"=>{"1"=>"root", "2"=>"1", "3"=>"2", "4"=>"2", "6"=>"2", "5"=>"2", "7"=>"2", "8"=>"1"}, "_"=>"1371679064349"}

      position_tracker = {}

      params['page'].each do |page_id,parent_id|
        next if parent_id == 'root'

        if position_tracker.has_key?(parent_id)
          position_tracker[parent_id] += 1
        else
          position_tracker[parent_id] = 1
        end

        page = PennStation::Page.find page_id.to_i
        parent = PennStation::Page.find parent_id.to_i
        page.parent_url = parent.url
        page.position = position_tracker[parent_id]
        page.save
      end
    end

    private

    def set_page
      @page = Page.find(params[:id])
    end

    def page_params
      params.require(:page).permit(:title, :ptype, :parent_url, :published, :show_in_nav,
                                   :seo_title, :seo_keywords, :seo_description)
    end
  end
end
