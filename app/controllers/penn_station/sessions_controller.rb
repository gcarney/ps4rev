require_dependency "penn_station/application_controller"

module PennStation
  class SessionsController < ApplicationController
    skip_before_filter :authenticate, :only => [:new, :create]

    def new
    end

    def create
      admin = Admin.where(email: params[:email]).first
      if admin && admin.authenticate(params[:password])
        session[:admin_id] = admin.id
        redirect_to root_url, :notice => "Logged in!"
      else
        flash.now.alert = "Invalid email or password"
        render "new"
      end
    end

    def destroy
      session[:admin_id] = nil
      redirect_to '/', :notice => "Logged out!"
    end
  end
end
