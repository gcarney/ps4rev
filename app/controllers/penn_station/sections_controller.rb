require_dependency "penn_station/application_controller"

module PennStation
  class SectionsController < ApplicationController
    before_action :set_page,    only: [:new, :show, :update]
#    before_action :set_section, only: [:update, :destroy]

    track_admin_activity

    def new
      @section = Section.new(page: @page)
    end

    def create
      @section = Section.new(section_params)

      if @section.save
        redirect_to edit_page_path(@section.page), notice: "Successfully created #{@section.name}."
      else
        render action: 'new'
      end
    end

    def edit
      @section = Section.find(params[:id])
      @page = @section.page
    end

    def update
      @section = @page.sections.find params[:id]
      if @section.update(section_params)

        # save or save and preview.

        if params[:save_op] == 'save'
          redirect_to edit_page_path(@page), notice: "Successfully updated section #{@section.name}."
        else
          redirect_to "#{@page.url}"
        end
      else
        render action: 'edit'
      end
    end

    def destroy
      @section = Section.find(params[:id])
      @page = @section.page
      @section.destroy
      redirect_to edit_page_path(@page), notice: "Section #{@section.name} was successfully deleted."
    end

    private

    def set_section
      @section = @page.sections.find_by_name section_params[:name]
    end

    def set_page
      @page = Page.find(params[:page_id] || section_params[:page_id])
    end

    def section_params
      params.require(:section).permit(:name, :content, :page_id, :moreinfo_url)
    end
  end
end
