require_dependency "penn_station/application_controller"

module PennStation
  class ImagePickerController < ApplicationController

    def index
      @folders = Folder.all.order(:name)
      @image_id = ""
      @folder_id = extract_folder_id params[:image_url]
      @uploaded_images = Folder.find(@folder_id).uploaded_files.select {|file| file.content_type.include? ('image')} unless @folder_id.blank?
      render :layout => false
    end

    def show
      @folder = Folder.find(params[:id])
      @uploaded_images = @folder.uploaded_files.select {|file| file.content_type.include? ('image')}
      render :layout => false     
    end


    private

    def extract_folder_id(url)
      matches = url.match /folders\/(\w+)\/([^\/]+)\.[^\/]+$/
      return "" unless matches
      folder_name = matches[1]
      folder = Folder.find_by_name(folder_name)
      folder.nil? ? "" : folder.id
    end

  end
end
