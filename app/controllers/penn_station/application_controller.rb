module PennStation
  class ApplicationController < ActionController::Base
    protect_from_forgery
    before_filter :authenticate

    private

    def authenticate
      unless session[:admin_id] and current_admin
        redirect_to login_url
      end
    end

    def current_admin
      @current_admin ||= Admin.find(session[:admin_id]) if session[:admin_id]
    end
    helper_method :current_admin

    def self.track_admin_activity(model_instance_var = nil)
      after_filter(:only => [:create, :update, :destroy]) do |controller|
        model_instance_var ||= controller.controller_name.singularize
        Activity.create(
          :administrator => @current_admin.name,
          :operation     => (controller.action_name == 'destroy' ? 'delete' : controller.action_name) + 'd',
          :info          => eval("@#{model_instance_var}").activity_info
        )
      end
    end
  end
end
