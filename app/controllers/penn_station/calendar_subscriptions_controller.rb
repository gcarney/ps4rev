require_dependency "penn_station/application_controller"

module PennStation
  class CalendarSubscriptionsController < ApplicationController
    before_action :set_calendar_subscription, only: [:show, :edit, :update, :destroy]
    before_action :set_page, only: [:new, :create]
    before_action :set_calendar, only: [:create]

#    track_admin_activity

    def index
      @calendar_subscriptions = CalendarSubscription.all.order(:name)
    end

    def show
    end

    def new
      @calendar_subscription = CalendarSubscription.new(page: @page)
    end

    def edit
    end

    def create
binding.pry
      if params[:calendar_name].empty?
        @calendar_subscription = CalendarSubscription.new(page: @page, calendar: @calendar)
      else
        begin
          @calendar = Calendar.create(name: params[:calendar_name])
          @calendar_subscription = CalendarSubscription.new(page: @page, calendar: @calendar)
        rescue
          render action: 'new'
        end
      end

      if @calendar_subscription.save
        redirect_to @calendar_subscription, notice: "Calendar #{@calendar_subscription.calendar.name} was successfully added."
      else
        render action: 'new'
      end
    end

    def update
      if @calendar_subscription.update(calendar_subscription_params)
        redirect_to @calendar_subscription, notice: "Successfully updated #{@calendar_subscription.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @calendar_subscription.destroy
      redirect_to calendar_subscriptions_url, notice: "calendar_subscriptionSubscription #{@calendar_subscription.name} was successfully deleted."
    end

    private

    def set_calendar_subscription
      @calendar_subscription = CalendarSubscription.find(params[:id])
    end

    def set_page
      @page = Page.find(calendar_subscription_params[:page_id])
    end

    def set_calendar
      @calendar = Calendar.find(params[:calendar_id])
    end

    def calendar_subscription_params
      params.require(:calendar_subscription).permit(:calendar_name, :page_id)
    end
  end
end
