module PennStation
  module EventsHelper
    def google_map_link(address, label = 'Map')
      base_url = 'https://maps.google.com/maps?q='
      encoded_location = [ address.address1,
                           address.address2,
                           address.city,
                           address.zipcode
                         ].join(' ').gsub(' ','+')

      content_tag(:a, label, href: "#{base_url}#{encoded_location}")
    end
  end
end
