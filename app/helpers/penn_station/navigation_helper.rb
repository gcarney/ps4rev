module PennStation
  module NavigationHelper

    def page_tree
      @page_tree ||= PennStation::PageTree.new # hits database.
    end

    def nav_list(start_page = nil, depth: 1, outer: 'ol', inner: 'li', current: nil)
      debeuggage = (
        "<!-- nav_list #{start_page.title.inspect}, \n" +
        "  depth = " + depth.inspect + "\n" +
        "-->"
      ).html_safe
      Rails.logger.debug debeuggage
      sub_page_list = page_tree.ordered_find(start_page)
      if sub_page_list.empty? and start_page.parent.url != "/"
        sub_page_list = page_tree.ordered_find(start_page.parent)
      end

      debeuggage +
      content_tag(:ol) do
        sub_page_list.inject(h '') do |result,page|
          (result + sub_nav(page,depth,page.content == current)).html_safe
        end
      end
    end

      # this is for the main site to use.
      # it puts the links pointing back into penn station on all main site pages.

    def admin_toolbar(page,user)
      buttons  = content_tag(:li,
                             link_to("Back to all pages", '/admin/pages'),
                             :id => "all_pages")

      edit_url = ((page.ptype == 'blog') && defined?(PennStationBlog)) ? "/admin/blog" : "/admin/pages/#{page.id}/edit"
      buttons += content_tag(:li,
                             link_to("Edit this page", edit_url),
                             :id => "edit") unless page.nil?

      links  = content_tag :li, link_to("My Account", "/admin/admins/#{user.id}")

      links += content_tag :li, link_to("Sign out", "/admin/logout")

      content_tag :div,
                  content_tag(:ul, buttons, :id => 'buttons') +
                  content_tag(:ul, links, :id => 'links') +
                  content_tag(:div, nil, :class => 'clear'),
                  :id => 'toolbar'
    end

    private

    def sub_nav(parent,depth, is_current = false)
      ordered_pages = page_tree.ordered_find(parent.content)
      Rails.logger.debug \
        "sub_nav of parent #{parent.content.title.inspect}, " +
        "ordered_pages size " + ordered_pages.size.inspect + "."
      class_str = is_current ? 'current' : ''
      result = content_tag(:li, class: class_str) do
        if depth > 1 and !ordered_pages.empty?
          nav_link(parent) +
          content_tag(:ol) do
            ordered_pages.inject(content_tag(:li,'',class: 'sub-nav-indicator')) do |result, subpage|
              if subpage.children.empty?
                (result + content_tag(:li, nav_link(subpage))).html_safe
              else
                (result + sub_nav(subpage, depth-1)).html_safe
              end
            end
          end
        else
          nav_link(parent)
        end
      end
      Rails.logger.debug \
        "Exit sub_nav #{parent.content.title.inspect} " +
        "giving #{result.inspect}."
      result
    end

    def nav_link(page)
      link_to(page.content.title, page.content.url)
    end
  end
end
