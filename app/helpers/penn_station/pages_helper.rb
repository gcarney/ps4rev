module PennStation
  module PagesHelper

    def page_tree_for_link_picker(root)
      page = root.content
      child_pages = root.children.sort_by{|s| s.content.position}.map do |node|
        page_tree_for_link_picker(node)
      end.join
      content_tag :ul, content_tag(:li, link_to(page.title, page.url, :class => '') + child_pages.html_safe)
    end

  end
end
