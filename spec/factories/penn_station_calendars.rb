
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_calendar, :class => 'PennStation::Calendar' do
    name "MyString"
  end
end
