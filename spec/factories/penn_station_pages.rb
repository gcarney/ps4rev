# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_page, :class => 'PennStation::Page' do
    title "MyString"
    url nil
    ptype "page"
    position 1
    published false
    show_in_nav false
  end
end
