# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_address, :class => 'PennStation::Address' do
    address1 "123 Main St"
    address2 "Suite 100"
    city "Baltimore"
    state "MD"
    zipcode "21229"
    directions "top of the stairs"
  end
end
