# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_location, :class => 'PennStation::Location' do
    name "MyString"
  end
end
