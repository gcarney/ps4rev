# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_slideshow, :class => 'PennStation::Slideshow' do
    name "MyString"
    image_size_note "size note"
  end
end
