

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_calendar_subscription, :class => 'PennStation::CalendarSubscription' do
    page nil
    calendar nil
  end
end
