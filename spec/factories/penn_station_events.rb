

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_event, :class => 'PennStation::Event' do
    name "MyString"
    summary "MyString"
    description "MyString"
  end
end
