# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_role, :class => 'PennStation::Role' do
    name "editor"
    admin nil
  end
end
