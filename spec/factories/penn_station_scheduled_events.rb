# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_scheduled_event, :class => 'PennStation::ScheduledEvent' do
    starts_at { Date.yesterday }
    ends_at   { Date.today }
    times     "1:00pm - 2:00pm"
  end
end

