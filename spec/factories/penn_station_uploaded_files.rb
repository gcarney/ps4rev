# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_uploaded_file, :class => 'PennStation::UploadedFile' do
    label "MyString"
    caption "MyText"
    description "MyText"
  end
end
