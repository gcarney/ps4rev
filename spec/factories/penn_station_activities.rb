# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_activity, :class => 'PennStation::Activity' do
    administrator "admin name"
    operation "created"
    info "page 'foo'"
  end
end
