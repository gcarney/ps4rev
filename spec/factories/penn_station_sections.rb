# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_section, :class => 'PennStation::Section' do
    name "main"
    content "main"
  end
end
