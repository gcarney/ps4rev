# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_admin, :class => 'PennStation::Admin' do
    email "joe@test.com"
    name "Joe Test"
    password "qwerty123"
    password_confirmation "qwerty123"
  end
end
