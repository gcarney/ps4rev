# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_folder, :class => 'PennStation::Folder' do
    name "MyString"
  end
end
