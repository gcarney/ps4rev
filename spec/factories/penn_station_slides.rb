# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_slide, :class => 'PennStation::Slide' do
    title "MyString"
    caption "MyString"
    description "MyString"
    url "MyString"
    publish false
    position 1

    image  { File.open(File.join(Rails.root, '/fixtures/files/check.png')) }
    size 1
    content_type "MyString"

    slideshow nil
  end
end
