require 'spec_helper'

module PennStation
  describe Admin do

    let(:admin) { FactoryGirl.build(:penn_station_admin) }

    context "when it is valid" do

      it "should have a name" do
        admin.should respond_to(:name)
      end

      it "should have a email" do
        admin.should respond_to(:email)
      end

      it "should have a password" do
        admin.should respond_to(:password)
      end

      it "should have a password_confirmation" do
        admin.should respond_to(:password_confirmation)
      end

      it "should have roles" do
        admin.should respond_to(:roles)
      end

      it "should be valid" do
        admin.should be_valid
      end
    end

    context "when it is not valid" do

      it "should fail when password is too short" do
        admin = FactoryGirl.build(:penn_station_admin, password: "1", password_confirmation: "1")
        admin.should_not be_valid
      end

      it "should fail when email is missing" do
        admin = FactoryGirl.build(:penn_station_admin, email: nil)
        admin.should_not be_valid
      end

      it "should fail when name is missing" do
        admin = FactoryGirl.build(:penn_station_admin, name: nil)
        admin.should_not be_valid
      end

      it "should fail when email is a duplicate" do
        admin = FactoryGirl.create(:penn_station_admin)
        admin_dup = FactoryGirl.build(:penn_station_admin)
        admin_dup.should_not be_valid
      end
    end

    it "should be able to tell if a user has a role" do
      admin.roles << FactoryGirl.build(:penn_station_role)
      admin.has_role?("admin").should be_false
    end
  end
end
