require 'spec_helper'

module PennStation
  describe Location do
    let(:location) { FactoryGirl.build(:penn_station_location) }

    context "when a location is valid" do

      it "should have a name" do
        location.should respond_to(:name)
      end

      it "should be valid" do
        location.should be_valid
      end
    end
  end
end
