require 'spec_helper'

module PennStation
  describe Slideshow do
    let(:slideshow) { FactoryGirl.build(:penn_station_slideshow) }

    context "when a slideshow is valid" do

      it "should have a name" do
        slideshow.should respond_to(:name)
      end

      it "should have a image_size_note" do
        slideshow.should respond_to(:image_size_note)
      end

      it "should have one or more slides" do
        slideshow.should respond_to(:slides)
      end

      it "should be valid" do
        slideshow.should be_valid
      end

      it "should return activity info" do
        slideshow.should respond_to(:activity_info)
      end
    end

    context "when slideshows are not valid" do

      it "should fail when the name is missing" do
        slideshow = FactoryGirl.build(:penn_station_slideshow, name: nil)
        slideshow.should_not be_valid
      end
    end

    context "when it is created" do
      let(:slideshow) { FactoryGirl.create(:penn_station_slideshow) }

      it "should return Activity record info" do
        expect(slideshow.activity_info).to eq("slideshow #{slideshow.name}")
      end
    end
  end
end
