require 'spec_helper'

module PennStation
  describe Page do

    let(:page) { FactoryGirl.build(:penn_station_page) }

    context "when a page is valid" do

      it "should have a title" do
        page.should respond_to(:title)
      end

      it "should have a url" do
        page.should respond_to(:url)
      end

      it "should have a parent_url" do
        page.should respond_to(:parent_url)
      end

      it "should have a position" do
        page.should respond_to(:position)
      end

      it "should have a published" do
        page.should respond_to(:published)
      end

      it "should have sections" do
        page.should respond_to(:sections)
      end

      it "should have a show_in_nav" do
        page.should respond_to(:show_in_nav)
      end

      it "should have a page type" do
        page.should respond_to(:ptype)
      end

      it "should be valid" do
        page.should be_valid
      end

      it "should return activity info" do
        page.should respond_to(:activity_info)
      end

      it "should have at least one section" do
        page.save
        page.sections.count.should == 1
      end
    end

    context "when pages are not valid" do

      it "should fail when the title is missing" do
        page = FactoryGirl.build(:penn_station_page, title: nil)
        page.should_not be_valid
      end

      it "should fail when the page type contains non-alphnumeric characters" do
        page = FactoryGirl.build(:penn_station_page, title:'good', ptype: 'A B')
        page.should_not be_valid
      end

      it "should fail when the title is a duplicate" do
        page = FactoryGirl.create(:penn_station_page)
        page_dup = FactoryGirl.build(:penn_station_page)
        page_dup.should_not be_valid
      end
    end

    context "a page" do
      let(:page_title) { 'the title' }

      context "when it is creates" do
        let(:p_url) { '/a' }
        let(:page) { FactoryGirl.create(:penn_station_page, title: page_title, parent_url: p_url) }

        it "should be locatable by url" do
          page2 = PennStation::Page.lookup page.url[1..-1]
          expect(page2.url).to eq(page.url)
        end

        it "should return Activity record info" do
          expect(page.activity_info).to eq("page #{page.title}")
        end
      end
    end

    context "a page's url" do
      let(:page_title) { 'the title' }

      context "when it is under the home page" do
        let(:p_url) { '/a' }
        let(:page) { FactoryGirl.create(:penn_station_page, title: page_title, parent_url: p_url) }

        it "should be looked up by url" do
          page2 = PennStation::Page.lookup page.url[1..-1]
          expect(page2.url).to eq(page.url)
        end

        it "should include the parent_url after saving" do
          page2 = PennStation::Page.find page.id
          expect(page2.url).to match(/^#{page2.parent_url}\/.+/)
        end

        it "should include the parent_url after loading" do
          page2 = PennStation::Page.find page.id
          expect(page2.parent_url).to eq(p_url)
        end
      end
    end

    context "when it is the HomePage" do
      let(:page_title) { 'HomePage' }
      let(:page) { FactoryGirl.create(:penn_station_page, title: page_title, parent_url: '') }

      it "should be published" do
        page2 = PennStation::Page.find page.id
        expect(page2.published).to be_true
      end

      it "should be of type 'HomePage'" do
        page2 = PennStation::Page.find page.id
        expect(page2.ptype).to eq("HomePage")
      end

      it "should be recognized as the home page" do
        page2 = PennStation::Page.find page.id
        expect(page2.home_page?).to be_true
      end

      context "url" do
        it "should be the root" do
          page2 = PennStation::Page.find page.id
          expect(page2.url).to eq("/")
        end
      end

      context "parent_url" do
        it "should be empty" do
          page2 = PennStation::Page.find page.id
          expect(page2.parent_url).to be_empty
        end
      end

      context "#parent" do
        let(:home_page) { FactoryGirl.create(:penn_station_page, title: 'Home Page', ptype: 'HomePage', parent_url: '') }
        let(:first_level_page) { FactoryGirl.create(:penn_station_page, title: 'About', parent_url: home_page.url) }

        it "first level pages should return the home page" do
          expect(first_level_page.parent).to eq(home_page)
        end
      end

      context ".top_level" do
        let(:home_page) { FactoryGirl.create(:penn_station_page, title: 'Home Page', ptype: 'HomePage', parent_url: '') }
        let(:first_level_page) { FactoryGirl.create(:penn_station_page, title: 'About', parent_url: home_page.url) }
        let(:second_level_page) { FactoryGirl.create(:penn_station_page, title: 'About Us', parent_url: first_level_page.url) }

        it "first level pages should return themselves" do
          expect(PennStation::Page.top_level(first_level_page)).to eq(first_level_page)
        end

        it "first level pages should return themsels" do
          expect(PennStation::Page.top_level(second_level_page)).to eq(first_level_page)
        end
      end
    end

    context "a first-level sub-page" do
      let(:page_title) { 'sub1' }
      let(:p_url) { '/' }
      let(:page) { FactoryGirl.create(:penn_station_page, title: page_title, parent_url: p_url) }

      it "should not be published" do
        page2 = PennStation::Page.find page.id
        expect(page2.published).to be_false
      end

      context "url" do
        it "should be under the root" do
          page2 = PennStation::Page.find page.id
          expect(page2.url).to eq("/sub1")
        end
      end

      context "parent_url" do
        it "should not be empty" do
          page2 = PennStation::Page.find page.id
          expect(page2.parent_url).to eq("/")
        end
      end
    end
  end
end
