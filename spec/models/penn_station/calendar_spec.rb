
require 'spec_helper'

module PennStation
  describe Calendar do
    let(:calendar) { FactoryGirl.build(:penn_station_calendar) }

    context "when a calendar is valid" do

      it "should have a name" do
        calendar.should respond_to(:name)
      end

      it "should have one or more pages" do
        calendar.should respond_to(:pages)
      end

      it "should have one or more scheduled events" do
        calendar.should respond_to(:scheduled_events)
      end

      it "should be valid" do
        calendar.should be_valid
      end
    end

    context "when calendars are not valid" do
      it "should fail when name is missing" do
        calendar = FactoryGirl.build(:penn_station_calendar, name: nil)
        calendar.should_not be_valid
      end
    end

    it "should allow a page to be associated" do
      page = FactoryGirl.build(:penn_station_page)
      calendar.pages = [page]
      calendar.save
      calendar.reload
      expect(calendar.pages).to eq([page])
    end

    it "should allow a scheduled event to be associated" do
      scheduled_event = FactoryGirl.build(:penn_station_scheduled_event)
      calendar.scheduled_events = [scheduled_event]
      calendar.save
      calendar.reload
      expect(calendar.scheduled_events).to eq([scheduled_event])
    end

    it "should return Activity record info" do
      page = FactoryGirl.create(:penn_station_page)
      calendar.pages << page
      expect(calendar.activity_info).to eq("calendar #{calendar.name}")
    end
  end
end
