require 'spec_helper'

module PennStation
  describe Address do
    let(:address) { FactoryGirl.build(:penn_station_address) }

    context "when it is valid" do

      it "should have an address1" do
        address.should respond_to(:address1)
      end

      it "should have an address2" do
        address.should respond_to(:address2)
      end

      it "should have a city" do
        address.should respond_to(:city)
      end

      it "should have a state" do
        address.should respond_to(:state)
      end

      it "should have a zipcode" do
        address.should respond_to(:zipcode)
      end

      it "should have directions" do
        address.should respond_to(:directions)
      end

      it "should be valid" do
        address.should be_valid
      end

      it "should return activity info" do
        address.should respond_to(:activity_info)
      end
    end

    context "when it is not valid" do
      it "should fail when there is no address1" do
        address.address1 = nil
        address.should_not be_valid
      end

      it "should fail when there is no city" do
        address.city = nil
        address.should_not be_valid
      end

      it "should fail when there is no state" do
        address.state = nil
        address.should_not be_valid
      end

      it "should fail when there is no zipcode" do
        address.zipcode = nil
        address.should_not be_valid
      end
    end

    context "when it is created" do
      let(:address) { FactoryGirl.create(:penn_station_address) }

      it "should return Activity record info" do
        expect(address.activity_info).to eq("address #{address.address1}")
      end
    end
  end
end
