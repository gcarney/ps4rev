require 'spec_helper'

module PennStation
  describe UploadedFile do
    let(:folder) { FactoryGirl.build(:penn_station_folder) }
    let(:uploaded_file) { FactoryGirl.build(:penn_station_uploaded_file, :folder => folder) }

    it "should return Activity record info" do
      expect(uploaded_file.activity_info).to eq("uploaded file to folder #{folder.name}")
    end
  end
end
