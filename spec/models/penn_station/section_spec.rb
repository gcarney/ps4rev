require 'spec_helper'

module PennStation
  describe Section do
    let(:section) { FactoryGirl.build(:penn_station_section) }

    context "when a section is valid" do

      it "should have a name" do
        section.should respond_to(:name)
      end

      it "should have content" do
        section.should respond_to(:content)
      end

      it "should return activity info" do
        section.should respond_to(:activity_info)
      end

      it "should be valid" do
        section.should be_valid
      end
    end


    context "when sections are not valid" do
      it "should fail when name is missing" do
        section = FactoryGirl.build(:penn_station_section, name: nil)
        section.should_not be_valid
      end

      it "should fail when content is missing" do
        section.save
        section.content = nil
        section.should_not be_valid
      end
    end

    it "should have a name of 'main'" do
      expect(section.name).to eq("main")
    end

    it "should return Activity record info" do
      page = FactoryGirl.build(:penn_station_page)
      section.page = page
      expect(section.activity_info).to eq("page #{page.title}, section #{section.name}")
    end
  end
end
