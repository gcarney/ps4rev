require 'spec_helper'

module PennStation
  describe Folder do
    let(:folder) { FactoryGirl.build(:penn_station_folder) }

    context "when a folder is valid" do

      it "should have a name" do
        folder.should respond_to(:name)
      end

      it "should be valid" do
        folder.should be_valid
      end

      it "should return activity info" do
        folder.should respond_to(:activity_info)
      end
    end

    context "when folders are not valid" do

      it "should fail when the name is missing" do
        folder = FactoryGirl.build(:penn_station_folder, name: nil)
        folder.should_not be_valid
      end
    end

    context "a folder" do
      context "when it is created" do
        let(:folder) { FactoryGirl.create(:penn_station_folder) }

        it "should return Activity record info" do
          expect(folder.activity_info).to eq("folder #{folder.name}")
        end
      end
    end
  end
end
