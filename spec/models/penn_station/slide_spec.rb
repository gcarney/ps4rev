require 'spec_helper'

module PennStation
  describe Slide do
    let(:slide) { FactoryGirl.build(:penn_station_slide) }

    context "when a slide is valid" do

      %w(
        title
        caption
        description
        url
        publish
        position
        slideshow
        image
        size
        content_type
      ).each do |attr|
        it "should have a #{attr}" do
          slide.should respond_to(attr.to_sym)
        end
      end

      it "should be valid" do
        slide.should be_valid
      end
    end

    context "when slides are not valid" do

      it "should fail when the title is missing" do
        slide = FactoryGirl.build(:penn_station_slide, title: nil)
        slide.should_not be_valid
      end
    end
  end
end
