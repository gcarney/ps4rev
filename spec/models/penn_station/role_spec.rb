require 'spec_helper'

module PennStation
  describe Role do
    let(:admin) { FactoryGirl.build(:penn_station_admin) }
    let(:role) { FactoryGirl.build(:penn_station_role, admin: admin) }
    let(:admin_role) { FactoryGirl.build(:penn_station_role, name: "admin", admin: admin) }
    let(:pennstation_admin_role) { FactoryGirl.build(:penn_station_role, name: "pennstation_admin", admin: admin) }

    context "when it is valid" do

      it "should have a name" do
        role.should respond_to(:name)
      end

      it "should have an admin" do
        role.should respond_to(:admin)
      end

      it "should be valid" do
        role.should be_valid
      end
    end

    context "when it is not valid" do
      it "should fail when there is no name" do
        role.name = nil
        role.should_not be_valid
      end

      it "should fail when the name is not in the list" do
        role.name = "no_role"
        role.should_not be_valid
      end
    end

    context "when it returns an assignable roles list" do
      it "should not return a list for editors" do
        admin.roles << role
        role.assignable_roles(admin).should be_empty
      end

      it "should return the proper list for admins" do
        admin.roles << role
        admin.roles << admin_role
        role.assignable_roles(admin).should eq([ "editor", "admin" ])
      end

      it "should return the proper list for pennstation_admins" do
        admin.roles << role
        admin.roles << admin_role
        admin.roles << pennstation_admin_role
        role.assignable_roles(admin).should eq(PennStation::Role::LIST)
      end
    end
  end
end
