require 'spec_helper'

module PennStation
  describe Activity do

    let(:activity) { FactoryGirl.build(:penn_station_activity) }

    context "when it is valid" do

      it "should have an administrator" do
        activity.should respond_to(:administrator)
      end

      it "should have an operation type" do
        activity.should respond_to(:operation)
      end

      it "should have info" do
        activity.should respond_to(:info)
      end

      it "should be valid" do
        activity.should be_valid
      end
    end

    context "when it is not valid" do

      it "should fail when administrator is missing" do
        activity = FactoryGirl.build(:penn_station_activity, administrator: nil)
        activity.should_not be_valid
      end

      it "should fail when operation is missing" do
        activity = FactoryGirl.build(:penn_station_activity, operation: nil)
        activity.should_not be_valid
      end

      it "should fail when info is missing" do
        activity = FactoryGirl.build(:penn_station_activity, info: nil)
        activity.should_not be_valid
      end
    end

    it "should return a message containing all info" do
      activity.message.should eq("#{activity.administrator} #{activity.operation} #{activity.info}")
    end
  end
end
