class PagesController < ApplicationController
  def show
    @page = PennStation::Page.find_by_url(params[:url])
  end
end

