require 'spec_helper'

module PennStation
  describe EventsHelper do

    describe "#google_map_link" do
      before(:all) {
        Address = Struct.new(:address1, :address2, :city, :state, :zipcode)
      }

      let(:address) { Address.new("3904 Hickory Avenue", "", "Baltimore", "MD", "21211") }
      let(:link) { "<a href=\"https://maps.google.com/maps?q=3904+Hickory+Avenue++Baltimore+21211\">Map</a>" }

      it "should return a google map anchor tag" do
         google_map_link(address).should eq(link)
      end

      it "should return a google map anchor tag with a custom label" do
         google_map_link(address,"Directions").should eq(link.gsub('Map','Directions'))
      end
    end
  end
end
