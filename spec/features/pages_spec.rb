require 'spec_helper'

describe "PennStation backend" do
  context "when not logged in" do
    it "should redirect to the login page" do
      visit "/admin/logout"
      visit "/admin/pages"
      page.should have_content("Login")
    end
  end

  context "when logged in as an admin" do
#    before(:each) { login }
    before(:each) do
      PennStation::Page.create(title: 'HomePage', ptype:'HomePage')
      admin = PennStation::Admin.create(name:'joe test', email: 'joe@test.com', password: 'qwerty123', password_confirmation: 'qwerty123')
      visit "/admin/logout"
      visit "/admin"
      fill_in 'Email', :with => admin.email
      fill_in 'Password', :with => admin.password
      click_button 'Log in'
    end

    describe "after logging in" do
      it "should display the pages index" do
        page.should have_content("Manage pages")
      end
    end

    describe "clicking the Pages tab" do
      it "should allow pages to be managed" do
        click_link 'Pages'
        page.should have_content("Manage pages")
      end

      describe "to create a page" do
        before(:each) do
          click_link 'Add a sub-page'
        end

        it "should display a form for creating a page" do
          page.should have_content("Create a new page")
        end

        it "should create a page when the form is filled out correctly" do
          fill_in 'Title', :with => 'zoo'
          find('#panel_ops #save').click
          page.find("#flash_messages").should have_content("Successfully")
        end

        it "should log the activity of creating a page" do
          num_activities = PennStation::Activity.count
          fill_in 'Title', :with => 'zoo'
          find('#panel_ops #save').click
          PennStation::Activity.count.should == num_activities + 1
        end

        it "should display an error when the form is not filled out correctly" do
          find('#panel_ops #save').click
          page.should have_content("Title can't be blank")
        end

        it "should display an error when the page title already exists" do
          fill_in 'Title', :with => 'zoo'
          find('#panel_ops #save').click

          click_link 'Pages'
          click_link 'Add a sub-page'

          fill_in 'Title', :with => 'zoo'
          find('#panel_ops #save').click

          page.should have_content("Title has already been taken")
        end
      end

      context "editing a page" do
        before(:each) do
          click_link 'Add a sub-page'
          fill_in 'Title', :with => 'zoo'
          find('#panel_ops #save').click
        end

        it "should display a form for editing a page" do
          page.should have_content("successfully")
        end

        it "should allow the page name to be changed" do
          fill_in 'Title', :with => 'zoo mod'
          find('#panel_ops #save').click
          page.should have_content("zoo mod")
        end

        it "should allow the page's main section to be changed" do
          find("#main_section").click
          find('#panel_ops #save').click
          page.should have_content("Successfully updated ")
        end

        it "should log the page's main section change" do
          num_activities = PennStation::Activity.count
          find("#main_section").click
          find('#panel_ops #save').click
          PennStation::Activity.count.should == num_activities + 1
        end

        describe "deleting a page" do
          it "should allow the page to delete the page" do
            visit "/admin/pages"
            click_link 'zoo'
            page.find(:css,'.delete_button').click
            page.should have_content("successfully")
          end
        end
      end
    end
  end
end

