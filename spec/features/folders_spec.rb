require 'spec_helper'

describe "PennStation backend" do
  context "when not logged in" do
    it "should redirect to the login page" do
      visit "/admin/logout"
      visit "/admin/folders"
      page.should have_content("Login")
    end
  end

  context "when logged in as an admin" do
#    before(:each) { login }
    before(:each) do
      PennStation::Page.create(title: 'HomePage', ptype:'HomePage')
      admin = PennStation::Admin.create(name:'joe test', email: 'joe@test.com', password: 'qwerty123', password_confirmation: 'qwerty123')
      visit "/admin/logout"
      visit "/admin"
      fill_in 'Email', :with => admin.email
      fill_in 'Password', :with => admin.password
      click_button 'Log in'
    end

    describe "clicking the Media tab" do
      it "should allow folders to be managed" do
        click_link 'Media'
        page.should have_content("Manage media")
      end

      describe "to create a folder" do
        before(:each) do
          click_link 'Media'
          click_link 'Add a new folder'
        end

        it "should display a form for creating a folder" do
          page.should have_content("Create a new folder")
        end

        it "should not have the file upload target" do
          page.should_not have_css('.dropzone')
        end

        it "should create a folder when the form is filled out correctly" do
          fill_in 'Name', :with => 'zoo'
          click_button ('Save')
          page.should have_content("successfully")
        end

        it "should log the activity of creating a folder" do
          num_activities = PennStation::Activity.count
          fill_in 'Name', :with => 'zoo'
          click_button ('Save')
          PennStation::Activity.count.should == num_activities + 1
        end

        it "should display an error when the form is not filled out correctly" do
          click_button('Save')
          page.should have_content("Form is invalid")
        end

        it "should display an error when the folder name already exists" do
          fill_in 'Name', :with => 'zoo'
          click_button('Save')

          click_link 'Media'
          click_link 'Add a new folder'

          fill_in 'Name', :with => 'zoo'
          click_button('Save')

          page.should have_content("Form is invalid")
        end
      end

      context "editing a page" do
        before(:each) do
          click_link 'Media'
          click_link 'Add a new folder'
          fill_in 'Name', :with => 'zoo'
          click_button('Save')
        end

        it "should display a form for editing a folder" do
          page.should have_content("successfully")
        end

        it "should allow the folder name to be changed" do
          fill_in 'Name', :with => 'zoo mod'
          click_button('Save')
          page.should have_content("zoo mod")
        end

        it "should log the folder's name change" do
          num_activities = PennStation::Activity.count
          fill_in 'Name', :with => 'zoo mod'
          click_button('Save')
          PennStation::Activity.count.should == num_activities + 1
        end

        describe "deleting a page" do
          it "should allow the user to delete the folder" do
            page.find(:css,'.delete_button').click
            page.should have_content("successfully")
          end
        end
      end
    end
  end
end

