require 'spec_helper'

describe "PennStation backend" do
  context "when not logged in" do
    it "should redirect to the login admin" do
      visit "/admin/logout"
      visit "/admin/admins"
      page.should have_content("Login")
    end
  end

  context "when logged in without the admin role" do
    before(:each) do
      PennStation::Page.create(title: 'HomePage')
      admin = PennStation::Admin.create!(name:'joe test', email: 'joe@test.com', password: 'qwerty123', password_confirmation: 'qwerty123')
      admin.roles = [ PennStation::Role.create(name:'editor') ]

      visit "/admin/logout"
      fill_in 'Email', :with => admin.email
      fill_in 'Password', :with => admin.password
      click_button 'Log in'
      visit "/admin/admins"
    end

    describe "clicking the administrators tab" do
      before(:each) do
        click_link 'Administrators'
      end

      it "should not allow admins to be managed" do
        page.should have_no_link("Add a new administrator")
      end
    end
  end

  context "when logged in with the admin role" do
#    before(:each) { login }
    before(:each) do
      PennStation::Page.create(title: 'HomePage')
      admin = PennStation::Admin.create!(name:'joe test', email: 'joe@test.com', password: 'qwerty123', password_confirmation: 'qwerty123')
      admin.roles = [ PennStation::Role.create(name:'editor'), PennStation::Role.create(name:'admin') ]

      visit "/admin/logout"
      fill_in 'Email', :with => admin.email
      fill_in 'Password', :with => admin.password
      click_button 'Log in'
      visit "/admin/admins"
    end

    describe "clicking the administrators tab" do
      before(:each) do
        click_link 'Administrators'
      end

      it "should allow admins to be managed" do
        page.should have_link("Add a new administrator")
      end

      describe "to create an administrator" do
        before(:each) do
          click_link 'Add a new administrator'
        end

        it "should display a form for creating a administrator" do
          page.should have_content("Create a new administrator")
        end

        it "should create a admin when the form is filled out correctly" do
          fill_in 'Email', :with => 'zoo@test.com'
          fill_in 'Name', :with => 'zoo keeper'
          fill_in 'Password', :with => 'zoo1xoo23'
          fill_in 'Password confirmation', :with => 'zoo1xoo23'
          click_button ('Save')
          page.should have_content("successfully")
        end

        it "should log the activity of creating a page" do
          num_activities = PennStation::Activity.count
          fill_in 'Email', :with => 'zoo@test.com'
          fill_in 'Name', :with => 'zoo keeper'
          fill_in 'Password', :with => 'zooxoo'
          fill_in 'Password confirmation', :with => 'zooxoo'
          click_button ('Save')
          PennStation::Activity.count.should == num_activities + 1
        end

        it "should display an error when the form is not filled out correctly" do
          click_button('Save')
          page.should have_content("Name can't be blank")
        end

        it "should display an error when the admin title already exists" do
          fill_in 'Email', :with => 'zoo@test.com'
          fill_in 'Name', :with => 'zoo keeper'
          fill_in 'Password', :with => 'zoo1xoo23'
          fill_in 'Password confirmation', :with => 'zoo1xoo23'
          click_button('Save')

          click_link 'Administrators'
          click_link 'Add a new administrator'

          fill_in 'Email', :with => 'zoo@test.com'
          fill_in 'Name', :with => 'zoo keeper'
          fill_in 'Password', :with => 'zooxoo'
          fill_in 'Password confirmation', :with => 'zooxoo'
          click_button('Save')

          page.should have_content("Email has already been taken")
        end
      end

      context "editing an administrator" do
        before(:each) do
          first('.index').click_link 'joe test'
        end

        it "should allow the administrator's name to be changed" do
          fill_in 'Name', :with => 'zoo supervisor'
          click_button('Save')
          page.should have_content("Successfully")
        end

        it "should not allow the administrator's name to be empty" do
          fill_in 'Name', :with => ''
          click_button('Save')
          page.should have_content("Name can't be blank")
        end

        describe "deleting an admin" do
          it "should be successful" do
            admin = PennStation::Admin.create!(name:'del test', email: 'del@test.com', password: 'qwerty123', password_confirmation: 'qwerty123')
            visit "/admin/admins"
            first('.index').click_link 'del test'
            page.find(:css,'.delete_button').click
            page.should have_content("successfully")
          end
        end
      end
    end
  end
end

