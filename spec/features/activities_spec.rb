require 'spec_helper'

describe "PennStation backend" do
  context "when logged in as an administrator" do
#    before(:each) { login }
    before(:each) do
      PennStation::Page.create(title: 'HomePage')
      admin = PennStation::Admin.create(name:'joe test', email: 'joe@test.com', password: 'qwerty123', password_confirmation: 'qwerty123')
      visit "/admin/logout"
      fill_in 'Email', :with => admin.email
      fill_in 'Password', :with => admin.password
      click_button 'Log in'
    end

    describe "clicking the dashboard tab" do
      it "should allow activities to be viewed" do
        click_link 'Dashboard'
        page.should have_content("Activity list")
      end
    end
  end
end

