# monkey patch to change default naming convention for uploaded file versions (thumbnails)
# from <versionname>_filename.ext to filename_<versionname>.ext
# see https://github.com/jnicklas/carrierwave/wiki/How-To%3A-Move-version-name-to-end-of-filename%2C-instead-of-front

module CarrierWave
  module Uploader
    module Versions
      def full_filename(for_file)
        parent_name = super(for_file)
        ext         = File.extname(parent_name)
        base_name   = parent_name.chomp(ext)
        [base_name, version_name].compact.join('_') + ext
      end

      def full_original_filename
        parent_name = super
        ext         = File.extname(parent_name)
        base_name   = parent_name.chomp(ext)
        [base_name, version_name].compact.join('_') + ext
      end
    end
  end
end

