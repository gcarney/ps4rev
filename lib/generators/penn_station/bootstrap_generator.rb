module PennStation
  module Generators
    class BootstrapGenerator < Rails::Generators::Base
      source_root File.expand_path("../../templates", __FILE__)
      desc "Bootstrap PennStation in a new client project"
      argument :organization, :type => :string

      def copy_skeleton
        %w( app db config ).each do |dir|
          directory dir,dir
        end
      end

      def complete
        puts "PennStation install complete!"
      end
    end
  end
end
