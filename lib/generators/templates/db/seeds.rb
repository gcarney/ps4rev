
admins = {
  'Developer' => {
    'email' => "developer@cctsbaltimore.org",
    'password' => "psadminpsadmin",
    'roles' => PennStation::Role::LIST

  },

  'Administrator' => {
    'email' => "admin@cctsbaltimore.org",
    'password' => "adminadmin",
    'roles' => PennStation::Role::LIST - [ 'pennstation_admin' ]
  },
}

admins.keys.each do |admin_name|
  an_admin = PennStation::Admin.where(:name => admin_name).first_or_create do |admin|
    puts "Adding admin #{admin_name}"
    admin.password = admins[admin_name]['password']
    admin.password_confirmation = admins[admin_name]['password']
    admin.email = admins[admin_name]['email']
  end

  admins[an_admin.name]['roles'].each do |role|
    PennStation::Role.where(:name => role).where(:admin_id => an_admin.id).first_or_create do |a_role|
      puts "  Adding admin role: #{role} to #{an_admin.name}"
      an_admin.roles << a_role
    end
  end

  puts
end

puts "Adding Home Page"
PennStation::Page.where(:ptype => 'HomePage').first_or_create do |page|
  page.title = 'Home Page'
  page.ptype = 'HomePage'
end

