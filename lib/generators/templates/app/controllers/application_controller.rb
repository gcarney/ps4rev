class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :current_admin
  helper ::PennStation::NavigationHelper
  helper ::PennStation::HeaderHelper

  private

  def current_admin
    @current_admin ||= PennStation::Admin.find(session[:admin_id]) if session[:admin_id]
  end
  helper_method :current_admin

end
