class PagesController < ApplicationController

  def show
    page = PennStation::Page.lookup(params[:url])

    if page.present?
      @page = page
    else
      render :layout => 'missing_page', :template => 'shared/missing_page'
    end
  end

  private

end

