module NavigationHelper
  def breadcrumb_trail(page, html_options = {}, page_anchor = nil)
    content_tag(:div, build_breadcrumb_trail(page,page_anchor), html_options)
  end

  private

  def build_breadcrumb_trail(page,page_anchor)
    [
      link_to('Home','/'),
      with_breadcrumb_parent_trail(page),
      with_breadcrumb_anchor(page,page_anchor)
    ].flatten.join(' > ').html_safe
  end

  def with_breadcrumb_parent_trail(page)
    p = page
    parent_url = page.parent_url
    result = []

    while p.parent_url != '/' do
      p = PennStation::Page.where(url: p.parent_url).first
      result.unshift(link_to(p.title,p.url))
    end

    result
  end

  def with_breadcrumb_anchor(p,anchor)
    anchor.blank? ? [ p.title ] : [ link_to(p.title,p.url), anchor ]
  end
end
